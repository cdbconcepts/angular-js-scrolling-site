<?php
/**
 * TODO: Move to separate file
 */

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$root = $protocol.$_SERVER['HTTP_HOST'];
$path = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$base_url = $root.$path;
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="mmgApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="mmgApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="mmgApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="mmgApp" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Match Marketing Group</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/all.css">
    <script src="assets/js/early.js"></script>
    <base href="<?php echo $base_url; ?>"/>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35172771-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <script>
        (function(d) {
            var config = {
                    kitId: 'wgu4ooi',
                    scriptTimeout: 3000
                },
                h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
        })(document);
    </script>
</head>
<body>

<!--
  <ul class="menu">
    <li><a href="/view1">view1</a></li>
    <li><a href="/view2">view2</a></li>
  </ul>

  <div ng-view></div>

  <div>Angular mmgApp app: v<span app-version></span></div>
-->

    <div id="mainNavFixed" class="container-fluid" ng-controller="NavCtrl">
        <div class="underlay" id="mainNavUnderlay"></div>
        <div class="page-container">
            <div class="row">
                <div class="col-md-14 col-md-offset-1 clearfix" id="mainNavCont">
                    <a class="match_logo" href="/" navscroll="page_1_padding"></a>
                    <!--
                    <nav role="navigation" id="pageNav">
                        <ul>
                            <li class="first"><a href="/who-we-are" navscroll="page_2">We Are</a></li>
                            <li><a href="/case-studies" navscroll="page_3">Case Studies</a></li>
                            <li><a href="/leadership"  navscroll="page_4">Leadership</a></li>
                            <li class="last"><a href="/contact" navscroll="page_5">Contact</a></li>
                        </ul>
                    </nav>
                    -->
                    <nav class="navbar navbar-match" id="pageNav">
                        <div>
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" ng-click="navbarCollapsed = !navbarCollapsed">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nv links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse"  collapse="navbarCollapsed">
                                <ul class="">
                                    <li class="first active"><a href="/who-we-are" navscroll="page_2" ng-click="navbarCollapsed = !navbarCollapsed">We Are</a></li>
                                    <li><a href="/case-studies" navscroll="page_3" ng-click="navbarCollapsed = !navbarCollapsed">Case Studies</a></li>
                                    <li><a href="/leadership"  navscroll="page_4" ng-click="navbarCollapsed = !navbarCollapsed">Leadership</a></li>
                                    <li class="last"><a href="/contact" navscroll="page_5" ng-click="navbarCollapsed = !navbarCollapsed">Contact</a></li>
                                </ul>

                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>



                </div>
            </div>
        </div>
    </div>

    <div id="page_1_padding" class="page-section full-height"></div>
    <section id="page_1" class="page-section full-height fixed"  ng-controller="LandingCtrl" scroll-position scroll-animation="navAnimation" scroll-animation-off="navOffAnimation" scroll-offset="100" scroll-hook="0">
        <div id="homeVideoCont" class="video-cont">
            <video id="homeVideo" class="video-js vjs-default-skin vjs-big-play-centered"
                   preload="auto" width="100%" height="100%" loop autoplay poster="/assets/img/home_bg.jpg" ng-style="style()" videoresize>
                <source src="/assets/video/Site_loop_sm.mp4" type="video/mp4"/>
                <source src="/assets/video/Site_loop_sm.webm" type="video/webm"/>
                <source src="/assets/video/Site_loop_sm.ogv" type="video/ogg"/>
                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a
                    web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports
                        HTML5 video</a></p>
            </video>
        </div>
        <div id="homeImgCont"></div>
        <div class="page-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-14 col-md-offset-1 text-right">
                        <div id="lockupContainer" verticalcenter="page_1" ng-style="vert_center_style()">
                            <div id="landingLockupLine">
                                <span class="helvetica-light">First to</span> <span class="garamond mg-blue italic">recognize</span><sup class="tm">&trade;</sup>
                            </div>
                            <div id="landingLockupCopy" class="helvetica-light">
                                We are more than an agency—we help brands realize<br/> the growth opportunities just
                                around the bend.
                            </div>
                            <div>
                                <a href="#" class="mmg-btn mmg-btn-large work-with-us-btn" navscroll="page_5">Work With Us &#8594;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="page_2" class="page-section full-height white-bg" ng-controller="AboutCtrl" scroll-position scroll-animation="aboutAnimation" scroll-offset="200" scroll-hook="0%">
        <div class="page-container">

            <div class="container-fluid">
                <header class="row" id="weAreHeader">
                    <div class="col-md-10 col-md-offset-3 text-center">
                        <h1><span class="garamond hello italic mg-blue">hello</span><br/><span class="sub-headline helvetica-light">We Are Match Marketing</span></h1>
                        <p class="copy helvetica-light">Welcome to the most innovative, digitally driven retail agency in North America. MatchMG is the <br/>First To Recognize&trade; the technology, insights, content and experiences that motivate consumers to connect with brands, stores and one another.</p>
                        <p class="digital-first mg-blue helvetica-bold">We are digital first, creative always.</p>
                    </div>
                </header>
                <div id="weAreBlocks">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-2 text-center">

                            <div class="block block-1" ng-repeat="company in companies">
                                <div class="underlay"></div>
                                <div class="content">
                                    <div class="icon-cont {{company.icon_class}}">
                                        <div class="icon icon_img"></div>
                                        <div class="icon icon_img_hover"></div>
                                    </div>
                                    <div class="block-copy">
                                        <div class="block-title"><p ng-bind-html="company.copy"></p></div>
                                        <div class="block-desc"><p ng-bind-html="company.copy_hover"></p></div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="page_3" class="page-section full-height" ng-controller="CaseStudyCtrl" scroll-position scroll-animation="csAnimation" scroll-animation-off="csOffAnimation" scroll-offset="" scroll-hook="50%">

        <div id="caseStudy" case-study="{{cs_index}}">
            <div class="hero-image" ng-style="{'background-image': 'url('+case_study.hero_img+ ')'}">
                <div class="overlay" id="heroOverlay"></div>
            </div>

            <div class="page-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-2">

                        <div class="row" id="caseDetail">
                            <div class="col-md-10" id="csItems">
                                <div class="row">
                                    <div class="col-md-8 col-xs-4 cs-item-cont" ng-repeat="cs_item in case_study.items">
                                        <a href="{{cs_item.high_res}}" colorbox="{close:'X',rel:'.cs-item',href:'{{cs_item.high_res}}' {{cs_item.type=='video' ? ',video:\'true\'' : ''}} }" data-video="{{cs_item.video_src}}" class="cs-item" ng-style="{'background-image': 'url('+cs_item.thumbnail+ ')'}">
                                            <span class="overlay"></span>
                                            <div class="plus-btn" ng-hide="cs_item.type=='video'">+</div>
                                            <div class="play-btn" ng-show="cs_item.type=='video'"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="csInfo">
                                <img id="csLogo" ng-src="{{case_study.logo_img}}"/>
                                <div id="csCopy" class="">
                                    <h2 class="garamond italic">{{case_study.title}}</h2>
                                    <div class="info_icon"><img ng-src="{{case_study.icon_img}}" /></div>
                                    <div class="description helvetica-light" ng-bind-html="case_study.description"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="slideControl" class="garamond italic">
            <div class="arrow-cont prev-arrow">
                <a class="arrow" ng-click="prev()" href="" slideout>
                    <span class="link-text">Prev</span>
                    <span class="hover-text">Prev: <span class="helvetica">{{case_study.prev_text}}</span></span>
                </a>
            </div>
            <div class="arrow-cont next-arrow">
                <a class="arrow" ng-click="next()" href="" slideout>
                    <span class="link-text">Next</span>
                    <span class="hover-text">Next: <span class="helvetica">{{case_study.next_text}}</span></span>
                </a>
            </div>
        </div>

    </section>

    <section id="page_4" class="page-section" ng-controller="TeamCtrl">
        <header id="leadershipHeader">
            <div class="page-container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-3">
                            <h2 class="garamond italic">Meet Our Leadership Team</h2>
                            <div class="brace-box"></div>
                    </div>
                </div>
            </div>
        </header>
        <div id="leadershipTeam">
            <div id="leadersContainer" class="leadership-height" positionslider>
                <div class="leader neg-skew" leader ng-repeat="employee in team" ng-class="{active : $first}" >
                    <div class="inner pos-skew leadership-height">
                        <div class="image-cont neg-skew">
                            <img class="pos-skew" ng-src="{{employee.image}}" alt="{{employee.name}}"/>
                        </div>
                        <div class="employee-info-cont neg-skew leadership-height">
                            <div class="employee-info pos-skew" verticalcenter="leadershipTeam" ng-style="vert_center_style()">
                                <div class="name garamond italic">{{employee.name}}</div>
                                <div ng-bind-html="employee.title" class="title helvetica-light"></div>
                            </div>
                        </div>
                    </div>
                    <div class="close-btn-cont pos-skew">
                        <a class="close-btn" href="#"></a>
                    </div>
                </div>
            </div>
        </div>

    </section>


<section id="page_5" class="page-section" ng-controller="ContactCtrl">

    <div id="offices" class="{{office.id}}">

        <div id="officeInfo" ng-style="{'background-image': 'url('+office.image+ ')'}">
            <div class="page-container">
                <div class="row" id="locationsRow">
                    <div class="col-md-10 col-md-offset-1 col-sm-7 col-sm-offset-1 helvetica-light" id="officeDetials">
                        <div id="locationsCol" verticalcenter="page_5" ng-style="vert_center_style()">
                            <h2>{{office.title}}</h2>
                            <div class="address" ng-bind-html="office.address_info"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-8">
                        <div id="contactFormCont">
                            <form novalidate id="contactForm" name="contactform" method="post" ng-submit="contactform.$valid && contactSubmit();">
                                <div ng-hide="formsuccess">
                                    <p class="title garamond italic">Send a Message</p>

                                    <div>
                                        <input type="text" placeholder="Full Name:" name="userName"
                                               ng-model="contactdetails.name"
                                               required/>

                                        <div ng-show="contactform.$submitted || contactform.userName.$touched"
                                             class="error_msg">
                                            <div ng-show="contactform.userName.$error.required">Tell us your name.</div>
                                        </div>
                                    </div>
                                    <div>
                                        <input type="email" placeholder="Email:" name="userEmail"
                                               ng-model="contactdetails.email" required/>

                                        <div ng-show="contactform.$submitted || contactform.userEmail.$touched"
                                             class="error_msg">
                                            <div ng-show="contactform.userEmail.$error.required">Please enter a valid
                                                email.
                                            </div>
                                        </div>
                                    </div>
                                    <div><textarea placeholder="Message:" ng-model="contactdetails.message"
                                                   name="userMessage" cols="30"
                                                   rows="10" required></textarea>

                                        <div ng-show="contactform.$submitted || contactform.userMessage.$touched"
                                             class="error_msg">
                                            <div ng-show="contactform.userMessage.$error.required">Please enter a
                                                message.
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <input class="helvetica-light" type="submit" value="Send &#8594;"/>
                                    </div>
                                </div>
                                <div ng-show="formsuccess" class="alert alert-success result_msg">
                                    Thank you, your inquiry has been submitted.
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid" id="officeTabsCont">
            <div class="page-container" id="officeTabs">
                <a class="tab {{(office.id == office_tab.id) ? 'active' : ''}}" href="#" ng-click="setOffice(office_tab.id)" navscroll="page_5" ng-repeat="office_tab in offices">{{office_tab.name}}</a>
            </div>
        </div>

    </div>

</section>


<footer>
    <div class="container">

        <h6>match mg</h6>
        <nav class="foot-nav">
            <ul>
                <li class="first"><a href="/who-we-are" navscroll="page_2">We Are</a></li>
                <li><a href="/case-studies" navscroll="page_3">Case Studies</a></li>
                <li><a href="/leadership" navscroll="page_4">Leadership</a></li>
                <li class="last"><a href="/contact" navscroll="page_5">Contact</a></li>
            </ul>
        </nav>
        <p>Copyright <?php echo date('Y'); ?> &copy; Match Marketing Group. All Rights Reserved.</p>

    </div>
</footer>



<div ng-view></div>

  <!-- In production use:
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
  -->
  <script src="assets/js/angular.js"></script>
  <script src="app.js"></script>
</body>
</html>
