<?php
/**
 * Created by PhpStorm.
 * User: Chris Boustead
 * Date: 9/25/14
 * Time: 3:56 PM
 */

$to         = 'chris.boustead@match-weld.com';
$cc         = 'chris.boustead@match-weld.com';
$from       = 'donotreply@match-weld.com';
$subject    = 'New Inquiry from MatchMG.com';

ini_set('date.timezone','America/New_York');

$data = json_decode(file_get_contents("php://input"));
foreach($data as $k => $post)
{
    $data->$k = substr(strip_tags($post),0,4000);  // max length to protect from sendgrid abuse
}

$name       = (!empty($data->name)) ? $data->name : 'Not provided';
$email      = (!empty($data->email)) ? $data->email : 'Not provided';
$message    = (!empty($data->message)) ? nl2br($data->message) : 'Not provided';


require_once('inc/Sendgrid.php');
$sg = new Sendgrid();
$msg = "<p>Inquiry at: ".date('F j, Y, g:i a')." on MatchMG.com</p>
<p>From: $name</p>
<p>Details:</p>";
$msg .= <<<EOT
<ul>
    <li><strong>Email:</strong> $email</li>
    <li><strong>Message:</strong> <p>$message</p></li>
</ul>
EOT;

//print_r($msg);

$mail = array(
    'to'=>$to,
    'cc'=>$cc,
    'from'=>$from,
    'subject'=>$subject,
    'html'=> $msg,
    'text'=> strip_tags($msg),
);
$res = $sg->mail($mail);

echo json_encode(array('result'=>$res));