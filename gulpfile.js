var gulp    = require('gulp');
var gutil   = require('gulp-util');
var uglify  = require('gulp-uglify');
var concat  = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var browserSync = require('browser-sync');
//var pngcrush = require('pngcrush');
var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');
    //closureCompiler = require('gulp-closure-compiler');



/**
 * Define some variables
 */
var jobs = ['js','early-js','css','fonts','swf','images','watch'];
var public_assets_path = 'public/assets/';
var bower_path = 'bower_components/';
var watch_folders = ['assets/css/*','assets/js/*','assets/img/*'];
var angular_watch_folders = ['assets/css/*','assets/js/*','assets/img/*','assets/ng/**'];

/**
 * Define all dependency paths
 */
var css_files = [
    'assets/css/*.css'
];
var early_js_files = [
    'assets/js/vendor/*.js'
    ,bower_path+'modernizr/modernizr.js'
];
var fonts = [
    bower_path + 'videojs/dist/video-js/font/*'
    ,'assets/fonts/*'
];
var swf = [
    bower_path + 'videojs/dist/video-js/video-js.swf'
];
var angular_js = [
    bower_path + 'angular/angular.js'
    ,bower_path + 'angular-route/angular-route.js'
    ,bower_path + 'angular-sanitize/angular-sanitize.js'
    ,bower_path + 'angular-bootstrap/ui-bootstrap.min.js'
    ,bower_path + 'videojs/dist/video-js/video.js'
    ,bower_path + 'gsap/src/minified/TweenMax.min.js'
    ,bower_path + 'gsap/src/minified/plugins/ScrollToPlugin.min.js'
    ,bower_path + 'colorbox/jquery.colorbox.js'
];
var angular_app_js = [
    'assets/ng/**/*.js',
    '!assets/ng/**/*_test.js'
];
var angular_app_html = [
    'assets/ng/**.html'
];
var js_files = [
    bower_path + 'gsap/src/minified/TweenMax.min.js'
    ,bower_path + 'videojs/dist/video-js/video.js'
    ,'assets/js/*.js'
];

var images = [
    'assets/img/**'
];


/**
 * Define all Jobs
 */

gulp.task('default', jobs);

gulp.task('css', function() {
    gulp.src(css_files)
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(concat('all.css'))
        .pipe(gulp.dest(public_assets_path+'css'));
});

gulp.task('fonts', function () {
    gulp.src(fonts)
        .pipe(gulp.dest(public_assets_path+'fonts'));
});

gulp.task('swf', function () {
    gulp.src(swf)
        .pipe(gulp.dest(public_assets_path+'swf'));
});

gulp.task('js', function () {
    gulp.src(js_files)
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest(public_assets_path+'js'));
});

/**
 * BEGIN: Angular Specific Jobs
 */

gulp.task('angular_js', function () {
    gulp.src(angular_js)
        .pipe(uglify())
        .pipe(concat('angular.js'))
        .pipe(gulp.dest(public_assets_path+'js'));

});

gulp.task('angular_app_js', function () {
    gulp.src(angular_app_js)
        .pipe(uglify({mangle:false}))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public'));
});

gulp.task('angular', ['angular_js', 'angular_app_js', 'early-js', 'css', 'images', 'fonts', 'browser-sync'], function () {

    gulp.watch(angular_watch_folders, function (e) {
        gulp.run(['angular_js', 'angular_app_js', 'early-js', 'css', 'images']);

        // Browser sync
        browserSync.notify('Local Files changed, wait for vagrant sync',2000);

    });

});
/**
 * END: Angular Specific Jobs
 */
gulp.task('browser-sync', function() {
    browserSync({
        files: ['public/*.html','public/assets/css/*'],
        proxy: "localhost:4567"
    });
});


gulp.task('early-js', function () {
    gulp.src(early_js_files)
        .pipe(uglify())
        .pipe(concat('early.js'))
        .pipe(gulp.dest(public_assets_path+'js'));
});

gulp.task('images', function () {
    /** TODO: fix image crush  */
    gulp.src(images,{ base:'./'})
        .pipe(pngcrush({ reduce: true })())
        .pipe(gulp.dest('public'));
});


/**
 * Watch all the jobs
 */
gulp.task('watch', function() {
    gulp.watch(watch_folders, function () {
        for(var i=0;i<jobs.length;++i)
        {
            gulp.run(jobs[i]);
        }
    });
});