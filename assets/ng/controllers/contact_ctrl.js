
mmgApp.controller('ContactCtrl', function ($scope, $http) {

    $scope.office_id = 'norwalk';

    var getOfficeById = function(id){

        for(var i = 0; i<$scope.offices.length;++i){
            if($scope.offices[i].id == id) return $scope.offices[i];
        }
        /** otherwise return the first office */
        return $scope.offices[0];
    };

    /**
     * Load the data
     */
    $http.get('data/office_locations.json')
        .then(function(res){
            $scope.offices = res.data;
            $scope.office = getOfficeById($scope.office_id);
        });


    $scope.setOffice = function(office_id){
        $scope.office = getOfficeById(office_id);
    };


    var form_error = function(msg)
    {
        msg = (!msg) ? 'There was an error, please try again.' : msg;
        $('.loader-container').hide();
        $('#contact_error').html(msg).css('display','').css('opacity',1);
        TweenMax.from($('#contact_error'),.5,{opacity: 0});

        setTimeout(function(){
            TweenMax.to($ct_form,.3,{opacity: 1});
            TweenMax.to($('#contact_error').css('display','none'),.5,{opacity: 0});
        },3000)

    };

    var form_success = function()
    {
        TweenMax.to($ct_form,.3,{opacity: 0, onComplete: function(){
            $('.loader-container, #contact_error').hide();
            $('#contact_result').html('Thank you for your inquiry.').css('display','');
            TweenMax.from($('#contact_result'),.5,{opacity: 0});

        }})
    };

    var contactdefault = {
        name : '',
        email : '',
        message : ''
    };

    $scope.contactdetails = contactdefault;


    $scope.formsuccess = false;

    $scope.contactSubmit = function(form){

        $http.post('contact.php',$scope.contactdetails)
            .success(function(data){
                console.log(data);
                $scope.formsuccess = true;
                $scope.contactdetails = {};
                $scope.apply();
            });
    }





});
