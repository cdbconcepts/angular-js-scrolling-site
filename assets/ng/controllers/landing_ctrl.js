
mmgApp.controller('LandingCtrl', function ($scope, $http) {


    var nav_underlay = document.getElementById('mainNavUnderlay');
    var nav_cont = document.getElementById('mainNavCont');

    $scope.navAnimation = function(el,directionX, directionY){

        new TimelineLite()
            .add([
                TweenMax.to(nav_underlay,.7,{height:'0', ease: Power3.easeOut}),
                TweenMax.to(nav_cont,.4,{paddingTop:'30px',paddingBottom:'30px'})

            ]);
    };

    $scope.navOffAnimation = function(el,directionX, directionY){

        new TimelineLite()
            .add([
                TweenMax.to(nav_underlay,.7,{height:'100%', ease: Power3.easeOut}),
                TweenMax.to(nav_cont,.4,{paddingTop:'5px',paddingBottom:'5px'})

            ]);

    }

});
