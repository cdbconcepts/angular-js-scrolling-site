
mmgApp.controller('AboutCtrl', function ($scope, $http) {


    /**
     * Load the data
     */
    $http.get('data/companies.json')
        .then(function(res){
            $scope.companies = res.data;
        });

    var about_header = document.getElementById('weAreHeader');

    var tl = new TimelineLite({paused: true});
    tl.add([
        TweenMax.from(about_header,.7,{opacity: 0, top: '-100px'})
    ]);


    $scope.aboutAnimation = function(el,directionX, directionY){

        tl.play();

        return true;

    }

});
