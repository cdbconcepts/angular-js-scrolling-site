
mmgApp.controller('CaseStudyCtrl', function ($scope, $http, ngDialog) {

    $scope.cs_index = 0;
    $scope.case_studies = [];

    /**
     * Load the data
     */
    $http.get('data/case_studies.json')
        .then(function(res){
            $scope.case_studies = res.data;
            $scope.case_study = res.data[0];

            /** Prev/Next Links */
            $scope.setLinks();
        });

    /**
     * LB "view"
     */
    $http.get('popup.html')
        .then(function(res){
            $scope.lb_html = res.data;
        });



    /** Case Study Animation */
    var cs_info = document.getElementById('csInfo');
    var tl = new TimelineLite({paused: true});
    tl.add([
        TweenMax.from(cs_info,1.5,{top: 150, opacity: 0, ease: Power3.easeOut})
    ]);

    /** Case Study Arrows */
    var arrows_visible = false;
    var cs_arrows = document.getElementById('slideControl');
    var tl_2 = new TimelineLite({paused: true});
    tl_2.add([
        TweenMax.from(cs_arrows,.3,{opacity: 0, onComplete: function(){ arrows_visible = true; }, onReverseComplete: function(){ arrows_visible = false; } })
    ]);

    $scope.csAnimation = function(el,directionX, directionY){
        tl.play();
        if(!arrows_visible) tl_2.play();
    };

    $scope.csOffAnimation = function(el,directionX, directionY){
        if(arrows_visible) tl_2.reverse();
    };


    /**
     * Case Study Change
     */
    var getSlideIndex = function(attempt){
        if (attempt > $scope.case_studies.length - 1) return 0;
        if (attempt < 0) return $scope.case_studies.length - 1;
        return attempt;
    };


    $scope.setLinks = function(){

        $scope.cs_prev_index = getSlideIndex($scope.cs_index - 1);
        $scope.cs_next_index = getSlideIndex($scope.cs_index + 1);

        var prev_study = $scope.getSlide($scope.cs_prev_index);
        var next_study = $scope.getSlide($scope.cs_next_index);

        $scope.case_study.next_text = next_study.title;
        $scope.case_study.prev_text = prev_study.title;

    }

    $scope.getSlide = function(i){
        return $scope.case_studies[i];
    };

    $scope.next = function(){
        $scope.cs_index = getSlideIndex($scope.cs_index + 1);
        $scope.changeStudy($scope.cs_index);
    };

    $scope.prev = function(){
        $scope.cs_index = getSlideIndex($scope.cs_index - 1);
        $scope.changeStudy($scope.cs_index);
    };

    $scope.orderProp = 'age';
});
