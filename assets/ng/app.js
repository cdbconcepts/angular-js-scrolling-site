'use strict';

var mmgApp = angular.module('mmgApp', [
    'ngRoute',
    'mmgApp.views.view1',
    'mmgApp.views.view2',
    'mmgApp.version',
    'ui.bootstrap',
    'ngSanitize'
]).
config(function($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);
    /* $routeProvider.otherwise({redirectTo: '/view1'}); */
    $routeProvider.otherwise(function(){return true;});
});
