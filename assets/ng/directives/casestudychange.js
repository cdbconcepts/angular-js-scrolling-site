
mmgApp.directive("caseStudy", function () {
    return function ($scope, $element, attrs) {

        var hero_overlay = document.getElementById('heroOverlay');
        var case_details = document.getElementById('caseDetail');

        $scope.heroOverlayInit = function(){
            return TweenMax.set(hero_overlay, {width:0, left:0});
        };

        $scope.heroOverlayInit();

        $scope.changeStudy = function(slide_index){
            var slide = $scope.getSlide(slide_index);

            $scope.heroOverlayInit();

            var tl = new TimelineLite({ onComplete: loadSlide, onCompleteParams: [slide], onCompleteScope: $element });
            tl.add([
                TweenMax.to(hero_overlay,.5, {width:'100%'}),
                TweenMax.to(case_details, 1, {opacity: 0})
            ]);

        };

        var loadSlide = function(slide){

            $scope.case_study = slide;

            /** Prev/Next Links */
            $scope.setLinks();

            TweenMax.to(case_details, 1, {opacity: 1});
            TweenMax.to(hero_overlay,.5, {left:'100%', width: 0});
            /** Update Scope */
            return $scope.$apply();
        };

    }
});