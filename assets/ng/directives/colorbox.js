mmgApp.directive('colorbox', ['$window',function($window) {


    var vid_html = function(vid){
        return '<video id="home_slide_vid" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="100%" height="100%" poster="'+vid.poster+'">'+
        '<source src="'+vid.mp4+'" type="video/mp4" />'+
        '<source src="'+vid.webm+'" type="video/webm" />'+
        '<source src="'+vid.ogv+'" type="video/ogg" />'+
        '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>'+
        '</video>'
    };

    return function($scope, $element, attrs){

        $element.bind('click',function(e){
            e.preventDefault();

            var cb_attrs = $scope.$eval(attrs.colorbox);

            if(cb_attrs.video)
            {

                var lb_vid;

                var video_attrs = $scope.$eval(attrs.video);
                var vid_markup = vid_html(video_attrs);

                cb_attrs.href = null;
                cb_attrs.html = vid_markup;
                cb_attrs.onComplete = function(){
                    videojs("home_slide_vid", {}, function(){
                        lb_vid = this;
                        lb_vid.play();
                        this.on("play", function(){
                            // Video Playing
                        });
                    });
                };
                cb_attrs.onCleanup = function(){
                    lb_vid.dispose();
                    lb_vid = null;
                    angular.element(document.getElementById('home_slide_vid')).remove();
                };
            }

            cb_attrs.maxWidth = function(){
                return angular.element($window).width() * .9;
            };

            $.colorbox(cb_attrs);
            return false;
        });

    };
}]);
