
mmgApp.directive('leader', ['$window', '$timeout', '$parse', function($window, $timeout, $parse) {
    return function($scope, $element, attrs) {

        var openLeader = function($el){
            $('.leader').removeClass('active');
            $el.addClass('active');
        };

        $element.bind('click',function(e){
            openLeader($element);
        });

        $($element[0]).find('.close-btn').bind('click','.close-btn',function(e){
            e.preventDefault();
            $('.leader').removeClass('active');
            return false;
        });
    };
}]);

