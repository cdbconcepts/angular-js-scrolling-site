mmgApp.directive('verticalcenter', function ($window) {
    return function (scope, element, attrs) {
        var w = angular.element($window);

        var center_within_id = attrs.verticalcenter;

        scope.getWindowDimensions = function () {
            return {
                'h': w.height(),
                'w': w.width()
            };
        };

        var vertCenter = function(oldValue,newValue){

            //console.log('vertcenter',element);

            var center_within = document.getElementById(center_within_id)

            var style = {
                top: ((center_within.clientHeight/2) - (element.outerHeight()/2))+ 'px',
                position: 'absolute'
            };

            scope.vert_center_style = function () {
                return style;
            };
        };

        scope.$watch(scope.getWindowDimensions, vertCenter, true);

        scope.$watch(function () { return element[0].innerHTML.length; }, vertCenter, true);





        w.bind('resize', function () {
            scope.$apply();
        });
    }
});