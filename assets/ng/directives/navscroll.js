
mmgApp.directive('navscroll', ['$window', '$timeout', '$parse', function($window, $timeout, $parse) {
    return function(scope, element, attrs) {


        element.bind('click',function(e){
            e.preventDefault();

            var go_to_id = document.getElementById(attrs.navscroll)
            var gotosection = angular.element(go_to_id);

            mainNavScroll(gotosection,$window);

        });

    };
}]);

function mainNavScroll ($el,$window){
    if(!$el[0]) return false;
    var padding = 0;
    var scroll_to_y = ($el[0].getBoundingClientRect().top + document.body.scrollTop) - padding;
    TweenMax.to(angular.element($window),.5,{scrollTo:{ y : scroll_to_y }, ease: Power3.easeOut });
}