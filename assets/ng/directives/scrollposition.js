mmgApp.directive('scrollPosition', ['$window', '$timeout', '$parse', function($window, $timeout, $parse) {
    return function(scope, element, attrs) {

        var windowEl = angular.element($window)[0];
        var directionMap = {
            "up": 1,
            "down": -1,
            "left": 1,
            "right": -1
        };

        var triggerEl = element;

        // better offset - top only
        var getOffset = function(attrs,triggerEl){
            if(!attrs.scrollOffset && !attrs.scrollHook) return 0;
            var el_height = triggerEl.outerHeight();
            var hook = attrs.scrollHook;
            var hook_offset = (hook) ? el_height - ((parseInt(hook)/100)*el_height) : el_height;
            var user_offset = (attrs.scrollOffset) ? attrs.scrollOffset : 0;
            return hook_offset - user_offset;

        };

        // We retrieve the element with the scroll
        scope.element = angular.element(element)[0];

        // We store all the elements that listen to this event
        windowEl._elementsList = $window._elementsList || [];
        windowEl._elementsList.push({element: scope.element, scope: scope, attrs: attrs, offset: getOffset(attrs,triggerEl)});

        var element, direction, index, model, scrollAnimationFunction, scrollAnimationFunctionOff, tmpYOffset = 0, tmpXOffset = 0;
        var userViewportOffset = 0;




        function triggerScrollFunctions() {

            for (var i = windowEl._elementsList.length - 1; i >= 0; i--) {
                element = windowEl._elementsList[i].element;
                if(!element.firedAnimation) {
                    var directionY = tmpYOffset - windowEl.pageYOffset > 0 ? "up" : "down";
                    var directionX = tmpXOffset - windowEl.pageXOffset > 0 ? "left" : "right";
                    tmpXOffset = windowEl.pageXOffset;
                    tmpYOffset = windowEl.pageYOffset;

                    //if(element.offsetTop - userViewportOffset < windowEl.pageYOffset && element.offsetHeight > (windowEl.pageYOffset - element.offsetTop)) {
                    if(element.offsetTop - windowEl._elementsList[i].offset < windowEl.pageYOffset && element.offsetHeight > (windowEl.pageYOffset - element.offsetTop)) {
                        model = $parse(windowEl._elementsList[i].attrs.scrollAnimation);
                        scrollAnimationFunction = model(windowEl._elementsList[i].scope);
                        windowEl._elementsList[i].scope.$apply(function() {
                            element.firedAnimation = scrollAnimationFunction(triggerEl, directionMap[directionX], directionMap[directionY]);
                        });
                        if(element.firedAnimation) {
                            windowEl._elementsList.splice(i, 1);
                        }
                    }else{
                        /** Fire "Off" function if exists */
                        if(windowEl._elementsList[i].attrs.scrollAnimationOff) {
                            model = $parse(windowEl._elementsList[i].attrs.scrollAnimationOff);
                            scrollAnimationFunctionOff = model(windowEl._elementsList[i].scope);
                            windowEl._elementsList[i].scope.$apply(function () {
                                element.firedAnimation = scrollAnimationFunctionOff(triggerEl, directionMap[directionX], directionMap[directionY]);
                            });
                            if (element.firedAnimation) {
                                windowEl._elementsList.splice(i, 1);
                            }
                        }
                    }
                } else {
                    index = windowEl._elementsList.indexOf(element); //TODO: Add indexOf polyfill for IE9
                    if(index > 0) windowEl._elementsList.splice(index, 1);
                }
            };
        };
        windowEl.onscroll = triggerScrollFunctions;
    };
}]);