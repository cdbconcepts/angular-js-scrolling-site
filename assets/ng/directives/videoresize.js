
var video_ratio = 16/9;

mmgApp.directive('videoresize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': w.height(),
                'w': w.width()
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {

            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            var vid_style = {
                height:0,
                width:0,
                marginTop:0,
                marginLeft:0
            };

            if(newValue.h*video_ratio < newValue.w){
                /**
                 * Black bars on sides
                 * So set a max height
                 */

                vid_style = {
                    height: newValue.w/video_ratio + 'px',
                    width: newValue.w + 'px',
                    marginTop: -(newValue.w/video_ratio)/2 + 'px',
                    marginLeft: -newValue.w/2 + 'px'
                }

            }else if (newValue.h*video_ratio > newValue.w){
                /**
                 * Black bars on top and bottom
                 * Set a max width
                 */

                vid_style = {
                    height: newValue.h + 'px',
                    width: newValue.h*video_ratio + 'px',
                    marginTop: -newValue.h/2 + 'px',
                    marginLeft: -(newValue.h*video_ratio)/2 + 'px'
                }


            }else{
                vid_style = {
                    height: newValue.h + 'px',
                    width: newValue.w + 'px',
                    marginTop: -newValue.h/2 + 'px',
                    marginLeft: -newValue.w/2 + 'px'
                }
            }

            scope.style = function () {
                return vid_style;
            };

        }, true);

        w.bind('resize', function () {
            console.log('apply')
            scope.$apply();
        });
    }
});