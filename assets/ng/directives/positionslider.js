
mmgApp.directive('positionslider', ['$window', '$timeout', '$parse', function($window, $timeout, $parse) {
    return function($scope, $element, attrs) {

        var slider = $element;
        var w = angular.element($window);

        /** Set Init CSS */
        $element.css('left',0);

        var moveSpeed = 0;
        var intervalTime = 30;


        var moveSlide = function(mouseout)
        {
            var l = parseInt($element.css('left'));
            if(l >= 0 && moveSpeed > 0){
                moveSpeed = 0;
                $element.css('left',0);
            }
            if(l <= -($element.width()*2) && moveSpeed < 0){
                moveSpeed = 0;
                $element.css('left',-($element.width()*2));
            }
            if(mouseout) {
                TweenMax.to($element, .7, {left: '+=' + moveSpeed + 'px', ease: Power3.easeOut});
            }else{
                TweenMax.to($element, .3, {left: '+=' + moveSpeed + 'px', ease: Linear.easeNone});
            }
        };

        var slideInterval = function(){
            return setInterval(function(){
                moveSlide();
            },intervalTime);
        };

        var intervalValue = null;

        var mousemove = function(e){

            /** Start the interval */
            var move_pct = (e.pageX/ w.width()) - .5;
            if(Math.abs(move_pct) < 0.25){
                move_pct = 0;
                return mouseout();
            }
            if(!intervalValue) intervalValue = slideInterval();
            moveSpeed = -move_pct * 200;
        };

        var mouseout = function(){
            moveSlide(true);
            clearInterval(intervalValue);
            intervalValue = null;
            moveSpeed = 0;
        };


        $element.bind('mousemove',mousemove);
        $element.bind('mouseout',mouseout);

    };
}]);

