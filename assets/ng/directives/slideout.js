mmgApp.directive('slideout', function() {
    return function ($scope, $element, attrs) {

        $scope.mouseover = function(){$element.parent().addClass('active')};
        $scope.mouseout = function(){$element.parent().removeClass('active')};

        $element.bind('mouseenter',$scope.mouseover);
        $element.bind('mouseleave',$scope.mouseout);
    }
});