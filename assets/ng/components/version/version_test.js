'use strict';

describe('mmgApp.version module', function() {
  beforeEach(module('mmgApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
