'use strict';

angular.module('mmgApp.version', [
  'mmgApp.version.interpolate-filter',
  'mmgApp.version.version-directive'
])

.value('version', '0.1');
