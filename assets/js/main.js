videojs.options.flash.swf = base_url+"assets/swf/video-js.swf";

$(document).ready(function(){


    /**
     * Music Animation
     */
    var $notesContainer = $('#notesContainer'),
        note_markup = function(){
            return '<div class="note note-' + random_integer(1,5) + '" />';
        };
    playNotes($notesContainer,note_markup);

    /**
     * Ornaments
     */
    var $ornamentsContainer = $('#companyIcons');
    ornamentsInit($ornamentsContainer);


    /**
     * Snow
     */
    var $snowArea = $('.canvas #snowArea');
    var $starsArea = $('.canvas #starsArea');
    if ($snowArea[0]) makeSnow($snowArea,{
        item: 'snow',
        mp : 100
    });
    if ($starsArea[0]) makeSnow($starsArea,{
        item: 'stars',
        mp : 25
    });

    /**
     * Video
     */
    var $vidOverlay = $('#videoOverlay')
    var overlayAni = TweenMax.fromTo($vidOverlay,.6,{opacity: 0},{opacity:.65, paused: true, onStart: function(){ $vidOverlay.css('display',''); }, onReverseComplete: function(){ $vidOverlay.css('display','none');  }  })
    var homeVid;
    videojs("main_vid", {}, function(){
        homeVid = this;
        this.on("play", function(){
            overlayAni.play();
        });
        this.on("pause", function(){
            overlayAni.reverse();
        });
        this.on("ended", function(){
            overlayAni.reverse();
        });
    });


});
/**
 *
 * @param $cont
 * @param markup
 */
function playNotes($cont,markup)
{
    setInterval(function(){
        $cont.createNote(markup);
    },1000)
}

/**
 *
 * @param note
 * @returns {*|jQuery|HTMLElement}
 */
$.fn.createNote = function(note) {
    var $note = $(note()).appendTo(this);
    var left = (random_integer(0,2) == 1) ? '-'+random_integer(0,50)+'px' : random_integer(0,50)+'px';
    var rotate = (random_integer(0,2) == 1) ? '-'+random_integer(0,20)+'deg' : random_integer(0,20)+'deg';
    var top = ($(window).width() <= 550) ? '-75px' : '-150px';
    new TimelineLite({ onComplete: function(){ $note.remove();}})
        .add([
            TweenMax.fromTo($note,2.8,{top:0,scale:.7},{top:top,scale:1.3,left: left,rotation:rotate, ease: Circ.easeOut})
        ])
        .add([
            TweenMax.to($note,1,{opacity:0})
        ],1.8);
    return $(this);
};

/**
 *
 * @param $cont
 */
function ornamentsInit($cont)
{
    var $ornaments = $cont.find('.comp_icon:visible');

    /**
     * First setup all 3D/perspective animation presets
     */
    TweenLite.set($ornaments, {perspective:500});
    TweenLite.set($ornaments.find('.logo'), {transformStyle:"preserve-3d"});
    TweenLite.set($ornaments.find('.back'), {rotationY:-180});
    TweenLite.set([$ornaments.find('.back'), $ornaments.find('.front')], {backfaceVisibility:"hidden"});

    /**
     * Set init animations
     */
    var ornTl = new TimelineLite({ paused : true });
    var initAnis = [];
    $ornaments.each(function(k,v){
        initAnis.push( TweenMax.from($(v).show(),.5,{top:'-150px',ease:Back.easeOut, delay: (random_integer(0,20)/10)}) );
    });
    ornTl.add(initAnis);
    ornTl.addLabel('initAniEnded');

    /**
     * Flip Ornaments to holiday Logos
     */
    var flipAnis = [];
    $ornaments.each(function(k,v){
        var rotation = (k % 2 == 1) ? '540_cw' : '180_cw';
        var time = (k % 2 == 1) ? 2 : 1.2;
        flipAnis.push( TweenLite.to($(v).find(".logo"), time, {rotationY:rotation, ease:Back.easeOut, delay: (random_integer(0,8)/10)})  );
    });
    ornTl.add(flipAnis,3);
    ornTl.addLabel('flipAniEnded');

    /**
     * Play the animation
     */
    ornTl.play();

}

/**
 *
 * @param low
 * @param high
 * @returns {*}
 */
function random_integer(low, high)
{
    return low + Math.floor(Math.random() * (high - low));
}


/**
 *
 * @param $area
 */
function makeSnow($area,config){

    //canvas init
    var canvas = $area[0];
    var ctx = canvas.getContext("2d");


    //canvas dimensions
    var W = window.innerWidth;
    var H = window.innerHeight;
    canvas.width = W;
    canvas.height = H;

    //snowflake particles
    var mp = config.mp; //max particles
    var particles = [];
    for(var i = 0; i < mp; i++)
    {
        particles.push({
            x: Math.random()*W, //x-coordinate
            y: Math.random()*H, //y-coordinate
            r: Math.random()*4+1, //radius
            d: Math.random()*mp //density
        })
    }

    //Lets draw the flakes
    function draw()
    {
        if(config.item == 'stars') {

            ctx.clearRect(0, 0, W, H);

            var imageObj = new Image();
            for (var i = 0; i < mp; i++) {
                var p = particles[i];

                imageObj.src = 'assets/img/stars_01.png';
                ctx.moveTo(p.x, p.y);
                ctx.drawImage(imageObj, p.x, p.y);
            }

            update();

        }else{

            ctx.clearRect(0, 0, W, H);

            ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
            ctx.beginPath();
            for (var i = 0; i < mp; i++) {
                var p = particles[i];

                ctx.moveTo(p.x, p.y);
                ctx.arc(p.x, p.y, p.r, 0, Math.PI * 2, true);


            }
            ctx.fill();
            update();

        }
    }

    //Function to move the snowflakes
    //angle will be an ongoing incremental flag. Sin and Cos functions will be applied to it to create vertical and horizontal movements of the flakes
    var angle = 0;
    function update()
    {
        angle += 0.01;
        for(var i = 0; i < mp; i++)
        {
            var p = particles[i];
            //Updating X and Y coordinates
            //We will add 1 to the cos function to prevent negative values which will lead flakes to move upwards
            //Every particle has its own density which can be used to make the downward movement different for each flake
            //Lets make it more random by adding in the radius
            p.y += Math.cos(angle+p.d) + 1 + p.r/2;
            p.x += Math.sin(angle) * 2;

            //Sending flakes back from the top when it exits
            //Lets make it a bit more organic and let flakes enter from the left and right also.
            if(p.x > W+5 || p.x < -5 || p.y > H)
            {
                if(i%3 > 0) //66.67% of the flakes
                {
                    particles[i] = {x: Math.random()*W, y: -10, r: p.r, d: p.d};
                }
                else
                {
                    //If the flake is exitting from the right
                    if(Math.sin(angle) > 0)
                    {
                        //Enter from the left
                        particles[i] = {x: -5, y: Math.random()*H, r: p.r, d: p.d};
                    }
                    else
                    {
                        //Enter from the right
                        particles[i] = {x: W+5, y: Math.random()*H, r: p.r, d: p.d};
                    }
                }
            }
        }
    }

    //animation loop
    setInterval(draw, 33);
}





